import React from 'react'
import './socialIcon.scss'

const SocialIcon = props => {
  return (
    <a
      className="social-icon"
      href={props.url}
      target="_blank"
      rel="noopener noreferrer"
    >
      {props.altText}
      <img src={props.icon} alt={props.altText} />
    </a>
  )
}

export default SocialIcon
