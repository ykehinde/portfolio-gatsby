import React, { useState } from 'react'
import scrollTo from 'gatsby-plugin-smoothscroll'
import Icon from 'assets/angle-up.svg'

import './backToTop.scss'

const BackToTop = () => {
  const [showScroll, setShowScroll] = useState(false)

  const checkScrollTop = () => {
    if (!showScroll && window.pageYOffset > 400) {
      setShowScroll(true)
    } else if (showScroll && window.pageYOffset <= 400) {
      setShowScroll(false)
    }
  }

  window.addEventListener('scroll', checkScrollTop)

  return (
    <>
      {showScroll ? (
        <button
          className="back-to-top-button"
          onClick={() => scrollTo('#___gatsby')}
        >
          <img src={Icon} alt="back to top" />
        </button>
      ) : null}
    </>
  )
}

export default React.memo(BackToTop)
