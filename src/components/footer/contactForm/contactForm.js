import React, { useState }  from "react"
import axios from "axios";

import "./contactForm.scss"

const ContactForm = () => {
    
    const [setServerState] = useState({
      submitting: false,
      status: null
    });
    const handleServerResponse = (ok, msg, form) => {
      setServerState({
        submitting: false,
        status: { ok, msg }
      });
      if (ok) {
        form.reset();
        notification(msg);
      }
    };
    const notification = (msg) => {
      alert(msg);
    };
    const handleOnSubmit = e => {
      e.preventDefault();
      const form = e.target;
      setServerState({ submitting: true });
      axios({
        method: "post",
        url: "https://getform.io/f/e00d694a-333c-47ef-a5db-4b3b15d708ec",
        data: new FormData(form)
      })
        .then(r => {
          handleServerResponse(
            true,
            "Thanks for your message!",
            form);
        })
        .catch(r => {
          handleServerResponse(false, r.response.data.error, form);
        });
    };
  return (
      <div className="contact-form">
          <h3>Get in contact</h3>
          <form onSubmit={handleOnSubmit}>
            <input className="form-input" type="email" name="email" placeholder="Your Email" required />
            <input className="form-input" type="text" name="name" placeholder="Your Name" required />
            <textarea className="form-input form-input--textarea" type="textarea" name="message" placeholder="Your Message" rows="3" />
            <input type="hidden" id="captchaResponse" name="g-recaptcha-response" />
            <button className="btn" type="submit">Send</button>
          </form>
      </div>
    );
  };
  
  export default ContactForm;

