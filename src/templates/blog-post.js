import React from 'react'
import { Link, graphql } from 'gatsby'
import parse from 'html-react-parser'

// We're using Gutenberg so we need the block styles
import '@wordpress/block-library/build-style/style.css'
import '@wordpress/block-library/build-style/theme.css'

import Layout from 'components/layout'
import SEO from 'components/seo'
import Footer from 'components/footer/footer'
import Header from 'components/header/header'

import './blog-post.scss'

const NoContent = () => {
  return (
    <>
      <p>Well This is awkward... There's no content for this post.</p>
      <Link to={'/'}>
        <p>Let's go home, shall we?</p>
      </Link>
    </>
  )
}

const BlogPostTemplate = ({ data: { previous, next, post } }) => {
  return (
    <Layout>
      <SEO title={post.title} description={post.excerpt} />

      <article
        className="blog-post"
        itemScope
        itemType="http://schema.org/Article"
      >
        <Header props={post} />

        {!!post.content ? (
          <section className="article-body" itemProp="articleBody">
            {parse(post.content)}
          </section>
        ) : (
          <section className="article-body" itemProp="articleBody">
            <NoContent />
          </section>
        )}

        <hr />
        <nav className="blog-post-nav">
          <ul>
            <li>
              {previous && (
                <Link to={previous.uri} rel="prev">
                  ← {parse(previous.title)}
                </Link>
              )}
            </li>

            <li>
              {next && (
                <Link to={next.uri} rel="next">
                  {parse(next.title)} →
                </Link>
              )}
            </li>
          </ul>
        </nav>
        <Footer />
      </article>
    </Layout>
  )
}

export default BlogPostTemplate

export const pageQuery = graphql`
  query BlogPostById(
    # these variables are passed in via createPage.pageContext in gatsby-node.js
    $id: String!
    $previousPostId: String
    $nextPostId: String
  ) {
    # selecting the current post by id
    post: wpPost(id: { eq: $id }) {
      id
      excerpt
      content
      title
      date(formatString: "MMMM DD, YYYY")

      featuredImage {
        node {
          altText
          localFile {
            childImageSharp {
              fluid(maxWidth: 1000, quality: 100) {
                ...GatsbyImageSharpFluid_tracedSVG
              }
            }
          }
        }
      }
    }

    # this gets us the previous post by id (if it exists)
    previous: wpPost(id: { eq: $previousPostId }) {
      uri
      title
    }

    # this gets us the next post by id (if it exists)
    next: wpPost(id: { eq: $nextPostId }) {
      uri
      title
    }
  }
`
