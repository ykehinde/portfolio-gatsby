import React from 'react'
import { Link } from 'gatsby'
import PropTypes from 'prop-types'

import PostImage from 'components/postImage/postImage'

import './gridItem.scss'

const GridItem = ({ props }) => {
  const { id, featuredImage, title, uri } = props

  const image = {
    fluid: featuredImage?.node?.sourceUrl,
    alt: featuredImage?.node?.altText || ``,
  }

  return (
    <li key={id} className="post-list-item grid-item">
      <Link to={uri} itemProp="uri">
        {image?.fluid && (
          <div className="post-image-container">
            <PostImage fluid={image.fluid} alt={image.alt} />
          </div>
        )}
        <h2 className="post-title" itemProp="headline">
          {title}
        </h2>
      </Link>
    </li>
  )
}

export default GridItem

GridItem.propTypes = {
  featuredImage: PropTypes.node,
  title: PropTypes.string,
  uri: PropTypes.string,
}
