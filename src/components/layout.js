import React, { useState, useEffect } from 'react'
import { Link, useStaticQuery, graphql } from 'gatsby'
import parse from 'html-react-parser'

import ThemeSwitch from './themeSwitch/themeSwitch'

import 'globals/index.scss'
import './layout.scss'

const Layout = ({ isHomePage, children }) => {
  const {
    wp: {
      generalSettings: { title },
    },
  } = useStaticQuery(graphql`
    query LayoutQuery {
      wp {
        generalSettings {
          title
          description
        }
      }
    }
  `)

  const initState = () => window.localStorage.getItem('theme') || 'light'

  const [theme, updateTheme] = useState(initState)

  const themeSwitch = () => {
    theme === 'dark' ? updateTheme('light') : updateTheme('dark')
  }

  useEffect(() => {
    window.localStorage.setItem('theme', theme)
  }, [theme])

  return (
    <div
      className={`global-wrapper ${theme} ${!isHomePage ? 'post-page' : ''}`}
      data-is-root-path={isHomePage}
    >
      <header className="global-header l-container">
        <h1 className={`main-heading`}>
          <Link to="/">{parse(title)}</Link>
        </h1>

        <ThemeSwitch onClick={themeSwitch} theme={theme} />
      </header>

      <main>{children}</main>
    </div>
  )
}

export default Layout
