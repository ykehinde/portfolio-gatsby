import React from 'react'
import ContactForm from 'components/footer/contactForm/contactForm'
import Socials from 'components/footer/socials/socials'

import './footer.scss'

const Footer = () => {
  return (
    <footer className="l-container">
      <ContactForm />
      <Socials />© {new Date().getFullYear()}
    </footer>
  )
}

export default Footer
