import React from 'react'
import './socials.scss'
import SocialIcon from 'components/socialIcon/socialIcon'

import GithubIcon from 'assets/socialIcons/social-github.svg'
import LinkedinIcon from 'assets/socialIcons/social-linkedin.svg'
import BitbucketIcon from 'assets/socialIcons/social-bitbucket.svg'

const Socials = () => {
  return (
    <div className="social-container">
      <SocialIcon
        url="http://www.google.com"
        icon={GithubIcon}
        altText="Github"
      />
      <SocialIcon
        url="http://www.google.com"
        icon={LinkedinIcon}
        altText="Linkedin"
      />
      <SocialIcon
        url="http://www.google.com"
        icon={BitbucketIcon}
        altText="Bitbucket"
      />
    </div>
  )
}

export default Socials
