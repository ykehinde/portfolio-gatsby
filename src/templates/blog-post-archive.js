import React from 'react'
import { Link, graphql } from 'gatsby'

import Bio from 'components/bio'
import Layout from 'components/layout'
import SEO from 'components/seo'
import BackToTop from 'components/backToTop/backToTop'
import AboutPage from 'pages/aboutPage'
import Footer from 'components/footer/footer'
import GridItem from 'components/gridItem/gridItem'

import 'components/grid.scss'

const BlogIndex = ({
  data,
  pageContext: { nextPagePath, previousPagePath },
}) => {
  const posts = data.allWpPost.nodes

  if (!posts.length || !posts) {
    return (
      <Layout isHomePage>
        <SEO title="All posts" />
        <Bio />
        <p>
          No blog posts found. Add posts to your WordPress site and they'll
          appear here!
        </p>
      </Layout>
    )
  }

  return (
    <Layout isHomePage>
      <SEO title="All posts" />
      <BackToTop />
      <AboutPage />
      <div className="grid">
        {posts &&
          posts.map(post => {
            const id = post.id
            return <GridItem key={id} props={post} />
          })}
      </div>

      {previousPagePath && (
        <>
          <Link to={previousPagePath}>Previous page</Link>
          <br />
        </>
      )}
      {nextPagePath && <Link to={nextPagePath}>Next page</Link>}
      <Footer />
    </Layout>
  )
}

export default BlogIndex

export const pageQuery = graphql`
  query WordPressPostArchive {
    allWpPost(sort: { fields: [date], order: DESC }) {
      nodes {
        id
        uri
        date(formatString: "MMMM DD, YYYY")
        title
        featuredImage {
          node {
            id
            mediaType
            sourceUrl
            title
          }
        }
      }
    }
  }
`
