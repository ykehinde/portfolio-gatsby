import React from 'react'
import './aboutPage.scss'

const keySkillList = [
  'React',
  'HTML + CSS',
  'SASS + LESS',
  'JavaScript Fundamentals + ES6',
  'Typescript',
  'Node',
  'Git',
  'Webpack',
  'Styled Components',
  'Responsive Design',
  'Accessibility',
  'Jest',
  'Angular',
  'React Testing Library',
  'GraphQL',
  'Apollo Client',
]

const AboutPage = () => {
  return (
    <div className="l-container">
      <h3>Brief</h3>
      <p>
        I am a diligent and hands-on front-end developer in London with about 8
        years experience and a flexible approach to work. I am highly motivated,
        enthusiastic, and goal-oriented with a positive attitude towards
        challenges in a working environment.
      </p>
      <h3>Key Skills</h3>
      <p>My key skills are as follows;</p>
      <ul className="key-skills">
        {keySkillList.map(skill => (
          <li key={skill}>{skill}</li>
        ))}
      </ul>
    </div>
  )
}

export default AboutPage
