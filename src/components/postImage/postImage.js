import React from 'react'
import './postImage.scss'

const PostImage = props => {
  console.log(props)
  return <img className="post-image" src={props.fluid} alt={props.altText} />
}

export default PostImage
