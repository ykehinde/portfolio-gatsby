import React from 'react'
import Image from 'gatsby-image'
import PropTypes from 'prop-types'

import './header.scss'

const Header = ({ props }) => {
  const { title, date, featuredImage } = props
  const image = {
    fluid: featuredImage?.node?.localFile?.childImageSharp?.fluid,
    alt: featuredImage?.node?.alt || ``,
  }
  return (
    <>
      <div className="l-container">
        <h2 className="main-heading" itemProp="headline">
          {title}
        </h2>
        <p>{date}</p>
      </div>

      <div className="header--image-container">
        {image && <Image fluid={image.fluid} alt={image.alt} />}
      </div>
    </>
  )
}

export default Header

Header.propTypes = {
  title: PropTypes.string,
  date: PropTypes.string,
  featuredImage: PropTypes.node,
}
