import React from 'react'
import './themeSwitch.scss'
import Sun from 'assets/sun.svg'
import Moon from 'assets/moon.svg'

const ThemeSwitch = ({ onClick, theme }) => {
  return (
    <button className="theme-switch" onClick={onClick}>
      {theme === 'light' ? (
        <img className={theme} alt={theme} src={Sun} />
      ) : (
        <img className={theme} alt={theme} src={Moon} />
      )}
    </button>
  )
}

export default ThemeSwitch
